{
    "$schema": "http://json-schema.org/draft-06/schema#",
    "schemaType": "subjectAccessRequest",
    "description": "Consumer Marketing data for Oath campaigns",
    "title": "Consumer Marketing Information",
    "properties": {
        "Product Usage": {
            "description": "General usage of Oath products and services",
            "title": "Oath Product Usage",
            "type": "object",
            "properties": {
                "Frontpage": {
                    "description": "General interest and usage of Yahoo Frontpage",
                    "title": "Usage of Yahoo Frontpage",
                    "type": "string",
                    "enum": [
                        "Heavy Usage",
                        "Regular Usage",
                        "Moderate Usage",
                        "Occasional Usage",
                        "Low Usage",
                        "No Activity",
                        "Unknown"
                    ]
                },
                "All Products": {
                    "description": "General interest and usage of all Yahoo products",
                    "title": "Usage Across All Yahoo products",
                    "type": "string",
                    "enum": [
                        "Heavy Usage",
                        "Regular Usage",
                        "Moderate Usage",
                        "Occasional Usage",
                        "Low Usage",
                        "No Activity",
                        "Unknown"
                    ]
                },
                "Finance": {
                    "description": "General interest and usage of Yahoo Finance",
                    "title": "Usage of Yahoo Finance",
                    "type": "string",
                    "enum": [
                        "Heavy Usage",
                        "Regular Usage",
                        "Moderate Usage",
                        "Occasional Usage",
                        "Low Usage",
                        "No Activity",
                        "Unknown"
                    ]
                },
                "News": {
                    "description": "General interest and usage of Yahoo News",
                    "title": "Usage of Yahoo News",
                    "type": "string",
                    "enum": [
                        "Heavy Usage",
                        "Regular Usage",
                        "Moderate Usage",
                        "Occasional Usage",
                        "Low Usage",
                        "No Activity",
                        "Unknown"
                    ]
                },
                "Search": {
                    "description": "General interest and usage of Yahoo Search",
                    "title": "Usage of Yahoo Search",
                    "type": "string",
                    "enum": [
                        "Heavy Usage",
                        "Regular Usage",
                        "Moderate Usage",
                        "Occasional Usage",
                        "Low Usage",
                        "No Activity",
                        "Unknown"
                    ]
                },
                "Lifestyle": {
                    "description": "General interest and usage of Yahoo Lifestyle",
                    "title": "Usage of Yahoo Lifestyle",
                    "type": "string",
                    "enum": [
                        "Heavy Usage",
                        "Regular Usage",
                        "Moderate Usage",
                        "Occasional Usage",
                        "Low Usage",
                        "No Activity",
                        "Unknown"
                    ]
                },
                "Sports": {
                    "description": "General interest and usage of Yahoo Sports",
                    "title": "Usage of Yahoo Sports",
                    "type": "string",
                    "enum": [
                        "Heavy Usage",
                        "Regular Usage",
                        "Moderate Usage",
                        "Occasional Usage",
                        "Low Usage",
                        "No Activity",
                        "Unknown"
                    ]
                }
            }
        },
        "Campaign Response": {
            "description": "Information about responses to email marketing campaigns",
            "title": "Campaign Response",
            "type": "object",
            "properties": {
                "Campaign Responses - all but Daily Yahoo": {
                    "description": "Oath Marketing campaign response information (excluding the Daily Yahoo newsletter)",
                    "title": "Campaign Responses - all but Daily Yahoo",
                    "type": "array",
                    "items": {
                        "type": "object",
                        "properties": {
                            "Campaign Date": {
                                "description": "Email marketing campaign date",
                                "title": "Campaign date",
                                "type": "string"
                            },
                            "Emails Opened": {
                                "description": "Total number of emails opened during the campaign",
                                "title": "Emails Opened",
                                "type": "number"
                            },
                            "Click Responses": {
                                "description": "Total number of click responses during the email campaign",
                                "title": "Click Responses",
                                "type": "number"
                            }
                        }
                    }
                },
                "Campaign Responses - Daily Yahoo": {
                    "description": "Oath Marketing campaign response information for the Daily Yahoo newsletter",
                    "title": "Campaign Responses - Daily Yahoo",
                    "type": "array",
                    "items": {
                        "type": "object",
                        "properties": {
                            "Campaign Date": {
                                "description": "Daily Yahoo email marketing campaign date",
                                "title": "Daily Yahoo Campaign Date",
                                "type": "string"
                            },
                            "Emails Opened": {
                                "description": "Total number of emails opened during the Daily Yahoo campaign",
                                "title": "Emails Opened",
                                "type": "number"
                            },
                            "Click Responses": {
                                "description": "Total number of click responses during the Daily Yahoo email campaign",
                                "title": "Click Responses",
                                "type": "number"
                            }
                        }
                    }
                }
            }
        },
        "Android Device": {
            "description": "Device usage indicates Android operating system",
            "title": "Android Device",
            "type": "string",
            "enum": [
                "true",
                "false"
            ]
        },
        "Best Product Offer": {
            "description": "Possible best product offering based on previous responses to marketing emails",
            "title": "Best Product Offer",
            "type": "string"
        },
        "Page Visits": {
            "description": "Oath web usage in the past 30, 60 and 90 days",
            "title": "Page Visits",
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "Previous 60 Days": {
                        "description": "Oath page visits in the last 60 days",
                        "title": "Page Visits - Last 60 Days",
                        "type": "string",
                        "enum": [
                            "Yes",
                            "No"
                        ]
                    },
                    "Previous 30 Days": {
                        "description": "Oath page visits in the last 30 days",
                        "title": "Page Visits - Last 30 Days",
                        "type": "string",
                        "enum": [
                            "Yes",
                            "No"
                        ]
                    },
                    "Page Address": {
                        "description": "The URL of the page",
                        "title": "Page Address",
                        "type": "string"
                    },
                    "Previous 90 Days": {
                        "description": "Oath page visits in the last 90 days",
                        "title": "Page Visits - Last 90 Days",
                        "type": "string",
                        "enum": [
                            "Yes",
                            "No"
                        ]
                    }
                }
            }
        },
        "Email Subscriptions": {
            "description": "Oath Marketing email subscriptions and preferences",
            "title": "Email subscriptions and preferences",
            "type": "object",
            "properties": {
                "Newsletter Preferences": {
                    "description": "Oath Newsletter subscription information",
                    "title": "Oath Newsletter Subscriptions",
                    "type": "array",
                    "items": {"type": "string"}
                },
                "Marketing Preferences": {
                    "description": "Oath Marketing email communication preferences",
                    "title": "Marketing Email Communications - Unsubscribed",
                    "type": "array",
                    "items": {"type": "string"}
                },
                "Offers": {
                    "description": "Marketing email subscription information",
                    "title": "Offer Subscriptions",
                    "type": "string",
                    "enum": [
                        "Yes",
                        "No"
                    ]
                }
            }
        },
        "Apple Device": {
            "description": "Device usage indicates Apple operating system",
            "title": "Apple Device",
            "type": "string",
            "enum": [
                "true",
                "false"
            ]
        },
        "Yahoo Network Activity": {
            "description": "Yahoo Network usage observed for 90 days",
            "title": "Yahoo Network Activity",
            "type": "string",
            "enum": [
                "Yes",
                "No"
            ]
        },
        "Email Suppressions": {
            "description": "Email suppressions lists (removal from Oath email marketing campaigns)",
            "title": "Email suppression lists",
            "type": "object",
            "properties": {
                "Do Not Mail List": {
                    "description": "The date and time when email address was added to the Do Not Mail suppression list (for exclusion from marketing emails)",
                    "title": "Do Not Mail suppression list",
                    "type": "string"
                },
                "Spamhaus": {
                    "description": "The date and time when the email address was added to the Spamhaus suppression list (for exclusion from marketing emails)",
                    "title": "Spamhaus Suppression Time",
                    "type": "string"
                },
                "Global Holdout": {
                    "description": "The date and time when the email address was added to the Global Holdout suppression list (for exclusion from marketing emails)",
                    "title": "Global Holdout Suppression Time",
                    "type": "string"
                },
                "Suppression Time": {
                    "description": "The date and time when the email address was added to the suppression list",
                    "title": "Suppression Time",
                    "type": "string"
                },
                "Suppression Email": {
                    "description": "The email address added to the suppression list (for exclusion from marketing emails)",
                    "title": "Suppression Email",
                    "type": "string"
                },
                "Quarterly Holdout": {
                    "description": "The date and time when the email address was added to the Quarterly Holdout suppression list (for exclusion from marketing emails)",
                    "title": "Quarterly Holdout Suppression Time",
                    "type": "string"
                },
                "Suppression Domains": {
                    "description": "The email domains in the suppression list",
                    "title": "Suppression Domains",
                    "type": "string"
                }
            }
        }
    },
    "$id": "http://www.yahoo.com/mkonepoint_subject_access_v9"
}