1. Categories of information My Yahoo! collects
   My Yahoo! may collect the following information:
      * Internet or Electronic Network Activity Information

2. Categories of sources where My Yahoo! collects information
   My Yahoo! may collect information from the following sources of information:
      * Verizon Media websites and apps

3. Reasons My Yahoo! collects and uses your information
   My Yahoo! may collect and use your information for the following reasons:
      * Performing services on behalf of Verizon Media

4. Categories of information My Yahoo! may disclose, or has disclosed in the past 12 months, for business purposes
   My Yahoo! has not disclosed your information for any business purposes.

5. Categories of information My Yahoo! may share, or has shared in the past 12 months, for commercial purposes
   My Yahoo! does not share your information for any commercial purposes.

