1. Categories of information LUUP collects
   LUUP may collect the following information:
      * Technical Identifiers
      * Geolocation Information
      * Advertising Inferences
      * Content Inferences

2. Categories of sources where LUUP collects information
   LUUP may collect information from the following sources of information:
      * Verizon Media websites and apps
      * Verizon Media Analytics Services
      * External Party Data Partners

3. Reasons LUUP collects and uses your information
   LUUP may collect and use your information for the following reasons:
      * Debugging
      * Short-term, transient use
      * Performing services on behalf of Verizon Media
      * Research
      * Quality Assurance and Service Improvements

4. Categories of information LUUP may disclose, or has disclosed in the past 12 months, for business purposes
   LUUP may disclose the following information for our business purposes:
      * Technical Identifiers
      * Geolocation Information
      * Advertising Inferences
      * Content Inferences
   LUUP may disclose these types of information for our business purposes with the following vendors and services providers:
      * Analytics Providers
      * Content Providers

5. Categories of information LUUP may share, or has shared in the past 12 months, for commercial purposes
   LUUP does not share your information for any commercial purposes.

