function init() {
    $("a#list_q").click(function() {
        loadQuestion();
    });
    $("a#list_a").click(function() {
        loadAnswer();
    });
}

function nl2br(str) {
    return str.replace(/([^>])\n/g, '$1<br/>\n');
}

var entityMap = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  "'": '&#39;',
  '/': '&#x2F;',
  '`': '&#x60;',
  '=': '&#x3D;'
};

function escapeHtml (string) {
  return String(string).replace(/[&<>"'`=\/]/g, function (s) {
    return entityMap[s];
  });
}

function loadQuestion() {
    html="";
    $.getJSON("./json/questions.json", function(json, status) {
        cnt=json.length;
        html+="Total: "+cnt+" question(s)\n";
        html+="<ul>\n";
        for(i=0; i<cnt; i++) {
            question=json[i];
            qid=question.id;
            title=escapeHtml(question.title);
            detail=nl2br(escapeHtml(question.detail));
            anscnt=question.answerCount;
            cat=question.category.name;
            date=question.created;

            upload = "";
            record = question.uploadRecord
            if (record) {
                if (record.mediaType==="IMAGE") {
                    upload = "<img src='" + record.mediaRecords[0].uri +
                             "'></img>";
                } else if (record.mediaType==="VIDEO") {
                    if (record.mediaRecords[0].spec.includes("MP4")) {
                        spec = "video/mp4";
                    } else {
                        spec = "video/webm";
                    }
                    upload = "<video controls><source type='" + spec + "' src='" +
                             record.mediaRecords[0].uri + "'/></video>";
                }
            }

            li=   "<li class='item'>";
            li+=    "<div>";
            li+=      "<h3><a href='https://answers.yahoo.com/question/index?qid="+qid+"' target='_blank'>"+title+"</a></h3>\n";
            li+="\n";
            li+=    "<div class='desc'>"+detail+"</div>";
            li+=    "<div class='upload'>"+upload+"</div>";
            li+=      "<div class='stat'>";
            li+=         anscnt+" answer(s)  ";
            li+=         "<span class='dot'>&middot;</span>";
            li+=         "  "+cat+"  ";
            li+=         "<span class='dot'>&middot;</span>";
            li+=         "  "+date;
            li+=      "</div>";
            li+=  "</li>";
            html+=li;
        }
        html+="</ul>\n";
        $("#content").html(html);
    });
}

function loadAnswer() {
    html="";
    $.getJSON("./json/answers.json", function(json, status) {
        cnt=json.length;
        html+="Total: "+cnt+" answer(s)\n";
        html+="<ul>\n";

        for(i=0; i<cnt; i++) {
            answer=json[i];
            qid=answer.id;
            title=escapeHtml(answer.title);
            detail=nl2br(escapeHtml(answer.userAnswer.text));
            anscnt=answer.answerCount;
            cat=answer.category.name;
            date=answer.created;

            li=   "<li class='item'>";
            li+=    "<div>";
            li+=      "<h3><a href='https://answers.yahoo.com/question/index?qid="+qid+"' target='_blank'>"+title+"</a></h3>\n";
            li+=      "<div class='desc'>"+detail+"</div>";
            li+=      "<div class='stat'>";
            li+=         anscnt+" answer(s)  ";
            li+=         "<span class='dot'>&middot;</span>";
            li+=         "  "+cat+"  ";
            li+=         "<span class='dot'>&middot;</span>";
            li+=         "  "+date;
            li+=      "</div>";
            li+=    "</div>";
            li+=  "</li>\n";
            html+=li;
        }
        html+="</ul>\n";
        $("#content").html(html);
    });
}
