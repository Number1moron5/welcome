1. Categories of information Your Account collects
   Your Account may collect the following information:
      * Contact Information
      * Geolocation Information
      * Technical Identifiers

2. Categories of sources where Your Account collects information
   Your Account may collect information from the following sources of information:
      * Verizon Media Analytics Services
      * Verizon Media websites and apps
      * Websites, apps or other public sources

3. Reasons Your Account collects and uses your information
   Your Account may collect and use your information for the following reasons:
      * Security
      * Delivering Content and Ads
      * Provide VM Services
      * Provide Personalized Content
      * Security and Fraud Prevention

4. Categories of information Your Account may disclose, or has disclosed in the past 12 months, for business purposes
   Your Account may disclose the following information for our business purposes:
      * Contact Information
      * Geolocation Information
      * Technical Identifiers
   Your Account may disclose these types of information for our business purposes with the following vendors and services providers:
      * Security External Parties
      * Social Button Providers

5. Categories of information Your Account may share, or has shared in the past 12 months, for commercial purposes
   Depending on the controls you activate in the Privacy Dashboard, and pursuant to the Verizon Media Privacy Policy, Your Account may share the following categories of information:
      * Contact Information
      * Geolocation Information
      * Technical Identifiers
   Depending on the controls you activate in the Privacy Dashboard, and pursuant to the Verizon Media Privacy Policy, Your Account may share information for the following purposes:
      * Security External Parties
      * Social Button Providers
   Depending on the controls you activate in the Privacy Dashboard, and pursuant to the Verizon Media Privacy Policy, Your Account may share this information with the following categories of businesses:
      * Delivering Content and Ads
      * Provide VM Services
      * Provide Personalized Content
      * Security and Fraud Prevention

